const {TransactionHandler} = require('sawtooth-sdk/processor/handler');
const {InvalidTransaction} = require('sawtooth-sdk/processor/exceptions');
const {TextEncoder, TextDecoder} = require('text-encoding/lib/encoding');
const {createHash} = require('crypto');

const FAMILY_NAME="hospital";
const FAMILY_VERSION="1.0";
const NAMESPACE = hash("Patient Record Management").substring(0,6);

var encoder = new TextEncoder('utf8');
var decoder = new TextDecoder('utf8');

function hash(v) {
    return createHash('sha512').update(v).digest('hex');
}

class Handler extends TransactionHandler {
    constructor(){
        super(FAMILY_NAME,[FAMILY_VERSION],[NAMESPACE]);
    }
    apply(transactionProcessRequest,context){
        try {
            let publicKey = transactionProcessRequest.header.signerPublicKey;
            let payloadObjNew = JSON.parse(decoder.decode(transactionProcessRequest.payload));
            switch (payloadObjNew.action) {
                case 'addNewRoutinePatient': {
                    // Validation for required fields
                    if(!payloadObjNew.dob) { throw new InvalidTransaction('Date of Birth required'); }
                    if(!payloadObjNew.patientName) { throw new InvalidTransaction('Patient name required'); }
                    if(!payloadObjNew.parentName) { throw new InvalidTransaction('Parent name required'); }
                    // Create routine patient address
                    let patientAddress = NAMESPACE + '0001' + payloadObjNew.dob + hash(payloadObjNew.patientName).substring(0,24) + hash(payloadObjNew.parentName).substring(0,24) + '000000';
                    return context.getState([patientAddress])
                    .then(res=>{
                        // Checking if address already exists
                        if(res[patientAddress].length !== 0) {
                            throw new InvalidTransaction('This patient already exists');
                        }
                        // Creating Custom Event
                        context.addEvent('hospital/addedRoutinePatient', [['publicKey', publicKey], ['dob', payloadObjNew.dob], ['patientName', payloadObjNew.patientName], ['parentName', payloadObjNew.parentName]], null);
                        // Entering data to state
                        let entries = {};
                        entries[patientAddress] = encoder.encode(JSON.stringify({
                            patientName: payloadObjNew.patientName,
                            dob: payloadObjNew.dob,
                            contactNo: payloadObjNew.contactNo,
                            address: payloadObjNew.address,
                            parentName: payloadObjNew.parentName,
                            govtID: payloadObjNew.govtID,
                            hospitalSection: payloadObjNew.hospitalSection
                        }));
                        return context.setState(entries)
                        .then(res=>{
                            let hospitalStateAddress = NAMESPACE + '0a' + hash(payloadObjNew.district).substring(0,30) + hash(payloadObjNew.hospitalName).substring(0,30);
                            // Linking to hospital In=patient department
                            if(res.includes(patientAddress)) {
                                if(payloadObjNew.hospitalSection) {
                                    if(payloadObjNew.hospitalSection == 'Epidemic') {
                                        hospitalStateAddress = hospitalStateAddress + 'ff';
                                    }
                                    // Getting hospital additional data
                                    return context.getState([hospitalStateAddress])
                                    .then(res=>{
                                        if(res[hospitalStateAddress].length == 0) {
                                            // Link patient address to hospital's additional data section
                                            entries[hospitalStateAddress] = encoder.encode(JSON.stringify([patientAddress]));
                                        } else {
                                            let payloadObjToState = JSON.parse(decoder.decode(res[hospitalStateAddress]));
                                            payloadObjToState.push(patientAddress);
                                            entries[hospitalStateAddress] = encoder.encode(JSON.stringify(payloadObjToState));
                                        }
                                        return context.setState(entries)
                                    });
                                }
                            }
                        });
                    });
                }
                case 'addNewEmergencyPatient': {
                    // Validation for required fields
                    if(!payloadObjNew.district) { throw new InvalidTransaction('Invalid user. Please log in.'); }
                    if(!payloadObjNew.hospitalName) { throw new InvalidTransaction('Invalid user. Please log in.'); }
                    if(!payloadObjNew.patientID) { throw new InvalidTransaction('Patient ID required.'); }
                    // Get hospital details
                    let hospitalAddress = NAMESPACE + '0a' + hash(payloadObjNew.district).substring(0,30) + hash(payloadObjNew.hospitalName).substring(0,30) + '00';
                    return context.getState([hospitalAddress])
                    .then(hospitalDetailsRes=> {
                        // Check if hospital details are present
                        if(hospitalDetailsRes[hospitalAddress].length == 0) {
                            throw new InvalidTransaction('Invalid User');
                        }
                        let hospitalObj = JSON.parse(decoder.decode(hospitalDetailsRes[hospitalAddress]));
                        // Create Emergency Patient Address
                        let patientAddress = NAMESPACE + '0000' + hash(hospitalObj.district).substring(0,18) + hash(hospitalObj.hospitalName).substring(0,18) + hash(payloadObjNew.patientID).substring(0,18) + '000000';
                        return context.getState([patientAddress])
                        .then(res=>{
                            // Checking if address already exists
                            if(res[patientAddress].length !== 0) {
                                throw new InvalidTransaction('This patient already exists');
                            }
                            let entries = {};
                            entries[patientAddress] = encoder.encode(JSON.stringify({
                                hospitalDistrict: hospitalObj.district,
                                hospitalName: hospitalObj.hospitalName,
                                patientID: payloadObjNew.patientID,
                                patientName: '',
                                dob: '',
                                contactNo: '',
                                address: '',
                                parentName: '',
                                govtID: '',
                                hospitalSection: payloadObjNew.hospitalSection
                            }));
                            return context.setState(entries)
                            .then(res=>{
                                let hospitalStateAddress = NAMESPACE + '0a' + hash(payloadObjNew.district).substring(0,30) + hash(payloadObjNew.hospitalName).substring(0,30);
                                // Linking to hospital In=patient department
                                if(res.includes(patientAddress)) {
                                    if(payloadObjNew.hospitalSection) {
                                        if(payloadObjNew.hospitalSection == 'Epidemic') {
                                            hospitalStateAddress = hospitalStateAddress + 'ff';
                                        }
                                        // Getting hospital additional data
                                        return context.getState([hospitalStateAddress])
                                        .then(res=>{
                                            if(res[hospitalStateAddress].length == 0) {
                                                // Link patient address to hospital's additional data section
                                                entries[hospitalStateAddress] = encoder.encode(JSON.stringify([patientAddress]));
                                            } else {
                                                let payloadObjToState = JSON.parse(decoder.decode(res[hospitalStateAddress]));
                                                payloadObjToState.push(patientAddress);
                                                entries[hospitalStateAddress] = encoder.encode(JSON.stringify(payloadObjToState));
                                            }
                                            return context.setState(entries)
                                        });
                                    }
                                }
                            });
                        });
                    });
                }
                case 'updateRoutinePatient': {
                    // Validation for required fields
                    if(!payloadObjNew.dob) { throw new InvalidTransaction('Patient date of birth required.'); }
                    if(!payloadObjNew.patientName) { throw new InvalidTransaction('Patient name required.'); }
                    if(!payloadObjNew.parentName) { throw new InvalidTransaction("Patient's Parent Name required."); }
                    let patientAddress = NAMESPACE + '0001' + payloadObjNew.dob + hash(payloadObjNew.patientName).substring(0,24) + hash(payloadObjNew.parentName).substring(0,24) + '000000';
                    return context.getState([patientAddress])
                    .then(res=>{
                        // Checking if address does not exist
                        if(res[patientAddress].length == 0) {
                            throw new InvalidTransaction('No such patient in the database');
                        }
                        // let payloadObjInState = JSON.parse(res[patientAddress]);
                        let payloadObjInState = JSON.parse(decoder.decode(res[patientAddress]));
                        let entries = {}, payloadObjToState = {};
                        // Check if address is pointing (linked) to another address
                        if(payloadObjInState.goto) {
                            return context.getState([payloadObjInState.goto])
                            .then(results=>{
                                if(results[payloadObjInState.goto].length == 0) {
                                    // Delete address which wrongly links to empty address
                                    return context.deleteState([patientAddress])
                                    .then(()=>{
                                        throw new InvalidTransaction('No such patient in the database');
                                    });
                                } else {
                                    // Adding new payload into linked Emergency Patient Address
                                    payloadObjToState = JSON.parse(results[payloadObjInState.goto]);
                                    payloadObjToState.contactNo = payloadObjNew.contactNo;
                                    payloadObjToState.address = payloadObjNew.address;
                                    payloadObjToState.govtID = payloadObjNew.govtID;
                                    payloadObjToState.hospitalSection = payloadObjNew.hospitalSection;
                                    entries[payloadObjInState.goto] = encoder.encode(JSON.stringify(payloadObjToState));
                                    return context.setState(entries)
                                    .then(result2=>{
                                        // Check if previous entry in hospital section
                                        if(payloadObjInState.hospitalSection == 'Epidemic') {
                                            let hospitalAddress = NAMESPACE + '0a' + hash(payloadObjNew.district).substring(0,30) + hash(payloadObjNew.hospitalName).substring(0,30) + 'ff';
                                            if(payloadObjInState.hospitalSection !== payloadObjNew.hospitalSection) {
                                                // Remove patient address from Hospital Additional Data
                                                return context.getState([hospitalAddress])
                                                .then(result3=>{
                                                    if(result3.length !== 0) {
                                                        let data = JSON.parse(decoder.decode(result3[hospitalAddress]));
                                                        let newData = data.filter(address => address !== payloadObjInState.goto);
                                                        let hospitalEntry = {};
                                                        hospitalEntry[hospitalAddress] = encoder.encode(JSON.stringify(newData));
                                                        return context.setState(hospitalEntry);
                                                    }
                                                });
                                            }
                                        }
                                    });
                                }
                            });
                        } else {
                            // Adding new payload into Routine Patient Adsress
                            payloadObjToState = payloadObjInState;
                            payloadObjToState.contactNo = payloadObjNew.contactNo;
                            payloadObjToState.address = payloadObjNew.address;
                            payloadObjToState.govtID = payloadObjNew.govtID;
                            payloadObjToState.hospitalSection = payloadObjNew.hospitalSection;
                            entries[patientAddress] = encoder.encode(JSON.stringify(payloadObjToState));
                            return context.setState(entries)
                            .then(result2=>{
                                // Check if previous entry in hospital section
                                if(payloadObjInState.hospitalSection == 'Epidemic') {
                                    let hospitalAddress = NAMESPACE + '0a' + hash(payloadObjNew.district).substring(0,30) + hash(payloadObjNew.hospitalName).substring(0,30) + 'ff';
                                    if(payloadObjInState.hospitalSection !== payloadObjNew.hospitalSection) {
                                        // Remove patient address from Hospital Additional Data
                                        return context.getState([hospitalAddress])
                                        .then(result3=>{
                                            if(result3.length !== 0) {
                                                let data = JSON.parse(decoder.decode(result3[hospitalAddress]));
                                                let newData = data.filter(address => address !== patientAddress);
                                                let hospitalEntry = {};
                                                hospitalEntry[hospitalAddress] = encoder.encode(JSON.stringify(newData));
                                                return context.setState(hospitalEntry);
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    });
                }
                case 'updateEmergencyPatient': {
                    let patientAddress = NAMESPACE + '0000' + hash(payloadObjNew.hospitalDistrict).substring(0,18) + hash(payloadObjNew.hospitalName).substring(0,18) + hash(payloadObjNew.patientID).substring(0,18) + '000000';
                    return context.getState([patientAddress])
                    .then(res=>{
                        // Checking if address does not exist
                        if(res[patientAddress].length == 0) {
                            throw new InvalidTransaction('No such patient in the database');
                        }
                        var payloadObjInStateString = decoder.decode(res[patientAddress]);
                        var payloadObjToState = JSON.parse(decoder.decode(res[patientAddress]));;
                        payloadObjToState.patientName = payloadObjNew.patientName;
                        payloadObjToState.dob = payloadObjNew.dob;
                        payloadObjToState.contactNo = payloadObjNew.contactNo;
                        payloadObjToState.address = payloadObjNew.address;
                        payloadObjToState.parentName = payloadObjNew.parentName;
                        payloadObjToState.govtID = payloadObjNew.govtID;
                        payloadObjToState.hospitalSection = payloadObjNew.hospitalSection;
                        let entries = {};
                        entries[patientAddress] = encoder.encode(JSON.stringify(payloadObjToState));
                        return context.setState(entries)
                        .then(results=>{
                            // Check if Emergency Patient can be linked as Routine Patient
                            if(payloadObjNew.dob && payloadObjNew.patientName && payloadObjNew.parentName) {
                                let routinePatientAddress = NAMESPACE + '0001' + payloadObjNew.dob + hash(payloadObjNew.patientName).substring(0,24) + hash(payloadObjNew.parentName).substring(0,24) + '000000';
                                let entries = {};
                                entries[routinePatientAddress] = encoder.encode(JSON.stringify({ goto: patientAddress }));
                                // Adding Transaction Receipt Data to link emergency and routine patient address
                                context.addReceiptData(encoder.encode('Linked Emergency Patient Address ' + patientAddress + ' to Routine Patient Address ' + routinePatientAddress));
                                return context.setState(entries);
                            }
                        })
                        .then(result2=>{
                            // Check if previous entry in hospital section
                            let payloadObjInState = JSON.parse(payloadObjInStateString);
                            if(payloadObjInState.hospitalSection == 'Epidemic') {
                                let hospitalAddress = NAMESPACE + '0a' + hash(payloadObjNew.district).substring(0,30) + hash(payloadObjNew.hospitalName).substring(0,30) + 'ff';
                                if(payloadObjInState.hospitalSection !== payloadObjNew.hospitalSection) {
                                    // Remove patient address from Hospital Additional Data
                                    return context.getState([hospitalAddress])
                                    .then(result3=>{
                                        if(result3.length !== 0) {
                                            let data = JSON.parse(decoder.decode(result3[hospitalAddress]));
                                            let newData = data.filter(address => address !== patientAddress);
                                            let hospitalEntry = {};
                                            hospitalEntry[hospitalAddress] = encoder.encode(JSON.stringify(newData));
                                            return context.setState(hospitalEntry);
                                        }
                                    });
                                }
                            } else if(payloadObjNew.hospitalSection == 'Epidemic') {
                                let hospitalAddress = NAMESPACE + '0a' + hash(payloadObjNew.district).substring(0,30) + hash(payloadObjNew.hospitalName).substring(0,30) + 'ff';
                                if(payloadObjInState.hospitalSection !== payloadObjNew.hospitalSection) {
                                    // Add patient address from Hospital Additional Data
                                    return context.getState([hospitalAddress])
                                    .then(result3=>{
                                        if(result3.length !== 0) {
                                            let data = JSON.parse(decoder.decode(result3[hospitalAddress]));
                                            data.push(patientAddress);
                                            let hospitalEntry = {};
                                            hospitalEntry[hospitalAddress] = encoder.encode(JSON.stringify(data));
                                            return context.setState(hospitalEntry);
                                        }
                                    });
                                }

                            }
                        });
                    });
                }
                case 'addVisitsDetails': {
                    // Validation for required fields
                    if(!payloadObjNew.patientAddress) { throw new InvalidTransaction('Patient blockchain address not specified'); }
                    if(!payloadObjNew.visitDate) { throw new InvalidTransaction('Date of visit required'); }
                    if(!payloadObjNew.visitReason) { throw new InvalidTransaction('Reason of visit required'); }
                    // Getting visit address from patient address
                    let address = payloadObjNew.patientAddress.replace(payloadObjNew.patientAddress.substring(0,8), payloadObjNew.patientAddress.substring(0,7) + '1');
                    address = address.substring(0,64) + payloadObjNew.visitDate;
                    // Get previous visit details
                    return context.getState([address])
                    .then(res=>{
                        let payloadToState = [];
                        if(res[address].length !== 0) {
                            payloadToState = JSON.parse(decoder.decode(res[address]));
                        }
                        let payloadNewObj = {
                            visitDate: payloadObjNew.visitDate,
                            visitReason: payloadObjNew.visitReason,
                            visitAction: payloadObjNew.visitAction,
                            visitID: payloadToState.length
                        };
                        payloadToState.push(payloadNewObj);
                        let entries = {};
                        entries[address] = encoder.encode(JSON.stringify(payloadToState));
                        return context.setState(entries);
                    });
                }
                case 'updateVisitsDetails': {
                    // Validation for required fields
                    if(!payloadObjNew.patientVisitAddress) { throw new InvalidTransaction('Patient blockchain Visits address not specified'); }
                    if(!payloadObjNew.visitID) { throw new InvalidTransaction('Patient blockchain Visit ID not specified'); }
                    if(!payloadObjNew.visitDate) { throw new InvalidTransaction('Date of visit required'); }
                    if(!payloadObjNew.visitReason) { throw new InvalidTransaction('Reason of visit required'); }
                    // Get previous visit details
                    return context.getState([payloadObjNew.patientVisitAddress])
                    .then(res=>{
                        // Check previous visits detail
                        let resObj = JSON.parse(new Buffer(res[payloadObjNew.patientVisitAddress],'base64').toString());
                        for(let visit of resObj) {
                            if(visit.visitID == payloadObjNew.visitID) {
                                visit.visitReason = payloadObjNew.visitReason;
                                visit.visitAction = payloadObjNew.visitAction;
                            }
                        }
                        let entries = {};
                        entries[payloadObjNew.patientVisitAddress] = encoder.encode(JSON.stringify(resObj));
                        return context.setState(entries);
                    });
                }
                case 'addTestsDetails': {
                    // Validation for required fields
                    if(!payloadObjNew.patientAddress) { throw new InvalidTransaction('Patient blockchain address not specified'); }
                    if(!payloadObjNew.testDate) { throw new InvalidTransaction('Date of test required'); }
                    if(!payloadObjNew.testType) { throw new InvalidTransaction('Type of test required'); }
                    // Getting test address from patient address
                    let address = payloadObjNew.patientAddress.replace(payloadObjNew.patientAddress.substring(0,8), payloadObjNew.patientAddress.substring(0,7) + '2');
                    address = address.substring(0,64) + payloadObjNew.testDate;
                    // Get previous test details
                    return context.getState([address])
                    .then(res=>{
                        let payloadToState = [];
                        if(res[address].length !== 0) {
                            payloadToState = JSON.parse(decoder.decode(res[address]));
                        }
                        let payloadNewObj = {
                            testDate: payloadObjNew.testDate,
                            testType: payloadObjNew.testType,
                            testResult: payloadObjNew.testResult,
                            testFilesURL: payloadObjNew.testFilesURL,
                            testID: payloadToState.length
                        };
                        payloadToState.push(payloadNewObj);
                        let entries = {};
                        entries[address] = encoder.encode(JSON.stringify(payloadToState));
                        return context.setState(entries);
                    });
                }
                case 'updateTestsDetails': {
                    // Validation for required fields
                    if(!payloadObjNew.patientTestAddress) { throw new InvalidTransaction('Patient blockchain Tests address not specified'); }
                    if(!payloadObjNew.testID) { throw new InvalidTransaction('Patient blockchain Test ID not specified'); }
                    if(!payloadObjNew.testDate) { throw new InvalidTransaction('Date of test required'); }
                    // Get previous test details
                    return context.getState([payloadObjNew.patientTestAddress])
                    .then(res=>{
                        // Check previous tests detail
                        let resObj = JSON.parse(new Buffer(res[payloadObjNew.patientTestAddress],'base64').toString());
                        for(let test of resObj) {
                            if(test.testID == payloadObjNew.testID) {
                                test.testType = payloadObjNew.testType;
                                test.testResult = payloadObjNew.testResult;
                                test.testFilesURL = payloadObjNew.testFilesURL;
                            }
                        }
                        let entries = {};
                        entries[payloadObjNew.patientTestAddress] = encoder.encode(JSON.stringify(resObj));
                        return context.setState(entries);
                    });
                }
                default:
                    throw new InvalidTransaction('Invalid action');
            }
        }
        catch(err){
            console.error(err);
        }
    }
}

module.exports = Handler