const {TransactionHandler} = require('sawtooth-sdk/processor/handler');
const {InvalidTransaction} = require('sawtooth-sdk/processor/exceptions');
const {TextEncoder, TextDecoder} = require('text-encoding/lib/encoding');
const {createHash} = require('crypto');

const FAMILY_NAME="non-hospital";
const FAMILY_VERSION="1.0";
const NAMESPACE = hash("Patient Record Management").substring(0,6);

var encoder = new TextEncoder('utf8');
var decoder = new TextDecoder('utf8');

function hash(v) {
    return createHash('sha512').update(v).digest('hex');
}

class Handler extends TransactionHandler {
    constructor(){
        super(FAMILY_NAME,[FAMILY_VERSION],[NAMESPACE]);
    }
    apply(transactionProcessRequest,context){
        try {
            let publicKey = transactionProcessRequest.header.signerPublicKey;
            let payloadObjNew = JSON.parse(decoder.decode(transactionProcessRequest.payload));
            console.log('NEWPAYLOADACTION: ' + payloadObjNew.action);
            switch (payloadObjNew.action) {
                case 'addNewHospital': {
                    // Validation for required fields
                    if(!payloadObjNew.district) { throw new InvalidTransaction('Hospital district required'); }
                    if(!payloadObjNew.hospitalName) { throw new InvalidTransaction('Hospital name required'); }
                    let hospitalAddress = NAMESPACE + '0a' + hash(payloadObjNew.district).substring(0,30) + hash(payloadObjNew.hospitalName).substring(0,30) + '00';
                    return context.getState([hospitalAddress])
                    .then(res=>{
                        // Checking if address already exists
                        if(res[hospitalAddress].length !== 0) {
                            throw new InvalidTransaction('This hospital already exists');
                        }
                        let entries = {};
                        entries[hospitalAddress] = encoder.encode(JSON.stringify({
                                                        district: payloadObjNew.district,
                                                        hospitalName: payloadObjNew.hospitalName,
                                                        contactNo: payloadObjNew.contactNo,
                                                        address: payloadObjNew.address,
                                                        registrationNo: payloadObjNew.registrationNo
                                                    }));
                        return context.setState(entries);
                    });
                }
                case 'deleteHospital': {
                    // Validation for required fields
                    if(!payloadObjNew.address) { throw new InvalidTransaction('Hospital state address required'); }
                    return context.deleteState([payloadObjNew.address]);
                }
                default:
                    throw new InvalidTransaction('Invalid action');
            }
        }
        catch(err){
            console.error(err);
        }
    }
}

module.exports = Handler