function logout() {
    document.cookie = "privateKey=;path=/;";
}

function isEmergency(status) {
    if(status) {
        document.getElementById('nonEmergency').style.display='none';
        document.getElementById('emergency').style.display='block';
    } else {
        document.getElementById('emergency').style.display='none';
        document.getElementById('nonEmergency').style.display='block';
    }
}