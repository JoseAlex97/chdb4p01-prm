var express = require('express');
var router = express.Router();

var {Client} = require('./client');

/* GET ROUTES */
/* Hospital Details */
router.get('/', function(req, res, next) {
  if(!req.cookies.privateKey) {
    res.redirect('/');
    return;
  }
  res.render('hospital');
});

/* Add Hospital Details */
router.get('/add', function(req, res, next) {
  if(!req.cookies.privateKey) {
    res.redirect('/');
    return;
  }
  res.render('hospitaladd');
});

/* Delete Hospital Details */
router.get('/delete', function(req, res, next) {
    if(!req.cookies.privateKey) {
      res.redirect('/');
      return;
    }
    let payload = {};
    payload.action = 'deleteHospital';
    payload.address = req.query.address;
    let client = new Client(req.cookies.privateKey);
    let deleteHospital = client.sendData(JSON.stringify(payload));
    deleteHospital.then(response=>{
        res.redirect('/hospital');
    });
  });

/* View Hospital Details */
router.get('/view', function(req, res, next) {
  if(!req.cookies.privateKey) {
    res.redirect('/');
    return;
  }
  let hospitalList = [];
  let client = new Client(req.cookies.privateKey);
  let hospitals = client.getHospitals(req.query.district, req.query.hospitalName);
  hospitals.then(response=>{
      let responseObj = JSON.parse(response);
    for(hospital of responseObj.data) {
        let hospitalObj = hospital;
        hospitalList.push(hospitalObj);
    }
    res.render('hospitalview', { hospitals : hospitalList });
  });  
});

/* Epidemic Report */
router.get('/epidemicReport', function(req, res, next) {
    if(!req.cookies.privateKey) {
        res.redirect('/');
        return;
    }
    let client = new Client(req.cookies.privateKey);
    let report = client.getEpidemicReport(req.query.district, req.query.hospitalName);
    report.then(results=>{
        resultsObj = JSON.parse(results);
        console.log('EPIDEMIC'+resultsObj);
        if(resultsObj.error && resultsObj.error.code == 75) {
            res.send('No epidemic patients in this hospital');
            return;
        }
        res.render('epidemicreport', { patients : resultsObj });
    });
});

/* POST ROUTES */
router.post('/add', function(req, res, next) {
  if(!req.cookies.privateKey) {
    res.redirect('/');
    return;
  }
  let payloadObj = req.body;
  payloadObj.action = 'addNewHospital';
  let client = new Client(req.cookies.privateKey);
  let sendData = client.sendData(JSON.stringify(payloadObj));
  sendData.then(result => {
      res.redirect('/hospital');
  });
});

module.exports = router;
