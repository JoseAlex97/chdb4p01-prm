function logout() {
    document.cookie = "privateKey=;path=/;";
    document.cookie = "district=;path=/;";
    document.cookie = "hospitalName=;path=/;";
}

function isEmergency(status) {
    if(status) {
        document.getElementById('nonEmergencyDiv').style.display='none';
        document.getElementById('emergencyDiv').style.display='block';
    } else {
        document.getElementById('emergencyDiv').style.display='none';
        document.getElementById('nonEmergencyDiv').style.display='block';
    }
    document.getElementById('emergencyLabel').innerHTML = 'Emergency Case';
}

function viewPatient(type) {
    if(document.getElementById('yes').checked || document.getElementById('no').checked) {
        let body = {};
        if(document.getElementById('yes').checked) {
            body = JSON.stringify({
                emergency: 'true',
                district: document.getElementById('district').value,
                hospitalName: document.getElementById('hospitalName').value,
                patientID: document.getElementById('patientID').value
            });
        } else {
            body = JSON.stringify({
                emergency: 'false',
                patientName: document.getElementById('patientName').value,
                dob: document.getElementById('dob').value,
                parentName: document.getElementById('parentName').value
            });
        }
        try {
            fetch('/patient', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: body
            })
            .then(res=>res.json())
            .then(res=>{
                if(type === 'info') {
                    location.href = '/patient/view?address=' + res.address;
                } else if(type === 'visits') {
                    location.href = '/patient/visits?address=' + res.address;
                } else if(type === 'tests') {
                    location.href = '/patient/tests?address=' + res.address;
                }
            });
        } catch (error) {
            console.error('Error:', error);
        }
    } else {
        document.getElementById('emergencyLabel').innerHTML = '<span style="color: red; font-weight: bold;">Please select</span> Emergency Case';
    }
}
