var express = require('express');
var router = express.Router();

var {Client} = require('./client');

/* GET ROUTES */
/* Patient */
router.get('/', function(req, res, next) {
  if(!req.cookies.privateKey) {
    res.redirect('/');
    return;
  }
  res.render('patient');
});

/* Add Patient Details */
router.get('/add', function(req, res, next) {
  if(!req.cookies.privateKey) {
    res.redirect('/');
    return;
  }
  res.render('patientadd');
});

/* View Patient Details */
router.get('/view', function(req, res, next) {
  if(!req.cookies.privateKey) {
    res.redirect('/');
    return;
  }
  let client = new Client(req.cookies.privateKey);
  let patientDetails = client.getDataFullAddress(req.query.address);
  patientDetails.then(result=>{
    let resultObj = JSON.parse(result);
    if(resultObj.data) {
      if(req.query.address.substring(8,10) === '00') {
        res.render('emergency-patient-view', JSON.parse(resultObj.data));
      } else if(req.query.address.substring(8,10) === '01') {
        res.render('routine-patient-view', JSON.parse(resultObj.data));
      }
    } else if (!resultObj.size) {
      res.send('No such patient in our database.');
    } else {
      res.send(JSON.stringify(resultObj));
    }
  });
});

/* View Patient Visits */
router.get('/visits', function(req, res, next) {
  if(!req.cookies.privateKey) {
    res.redirect('/');
    return;
  }
  let client = new Client(req.cookies.privateKey);
  let patientDetails = client.getVisitsInfo(req.query.address);
  patientDetails.then(result=>{
    let resultObj = JSON.parse(result);
    let renderObj = JSON.parse(resultObj.patient.data);
    renderObj.visits = resultObj.visits;
    res.render('patientvisits', renderObj);
    return;
  });
});

/* Add Patient Visits */
router.get('/visits/add', function(req, res, next) {
    if(!req.cookies.privateKey) {
        res.redirect('/');
        return;
    }
    res.render('patientvisitsadd');
    return;
});

/* Update Patient Visits */
router.get('/visits/update', function(req, res, next) {
    if(!req.cookies.privateKey) {
        res.redirect('/');
        return;
    }
    let client = new Client(req.cookies.privateKey);
    let clientDetails = client.getDataPatientVisit(req.query.address, req.query.visitID);
    clientDetails.then(result=>{
        result.patientVisitAddress = req.query.address;
        res.render('patientvisitsupdate', result);
        return;
    });
});

/* View Patient Tests */
router.get('/tests', function(req, res, next) {
    if(!req.cookies.privateKey) {
      res.redirect('/');
      return;
    }
    let client = new Client(req.cookies.privateKey);
    let patientDetails = client.getTestsInfo(req.query.address);
    patientDetails.then(result=>{
      let resultObj = JSON.parse(result);
      let renderObj = JSON.parse(resultObj.patient.data);
      renderObj.tests = resultObj.tests;
      res.render('patienttests', renderObj);
      return;
    });
  });

/* Add Patient Tests */
router.get('/tests/add', function(req, res, next) {
    if(!req.cookies.privateKey) {
        res.redirect('/');
        return;
    }
    res.render('patienttestsadd');
    return;
});

/* Update Patient Tests */
router.get('/tests/update', function(req, res, next) {
    if(!req.cookies.privateKey) {
        res.redirect('/');
        return;
    }
    let client = new Client(req.cookies.privateKey);
    let clientDetails = client.getDataPatientTest(req.query.address, req.query.testID);
    clientDetails.then(result=>{
        result.patientTestAddress = req.query.address;
        res.render('patienttestsupdate', result);
        return;
    });
});


/* POST ROUTES */
/* Patient */
router.post('/', function(req, res, next) {
    try {
        if(!req.cookies.privateKey) {
            res.redirect('/');
            return;
          }
          let client = new Client(req.cookies.privateKey);
          if(req.body.emergency === 'true') {
            let getEmergencyPatientAddress = client.getEmergencyPatientAddress(req.body.district, req.body.hospitalName, req.body.patientID);
            res.send(JSON.stringify({address: getEmergencyPatientAddress}));
          } else if(req.body.emergency === 'false') {
            let getRoutinePatientAddress = client.getRoutinePatientAddress(req.body.patientName, req.body.dob, req.body.parentName);
            res.send(JSON.stringify({address: getRoutinePatientAddress}));
          }
    }
    catch(error) {
        console.log(error);
    }  
});

/* Add Patient Details */
router.post('/add', function(req, res, next) {
  if(!req.cookies.privateKey) {
    res.redirect('/');
    return;
  }
  let payloadObj = req.body;
  payloadObj.district = req.cookies.district;
  payloadObj.hospitalName = req.cookies.hospitalName;
  if(payloadObj.emergency == 'true') {
    payloadObj.action = "addNewEmergencyPatient";
  } else if(payloadObj.emergency == 'false') {
    payloadObj.action = "addNewRoutinePatient";
  }
  if(payloadObj.action) {
    let client = new Client(req.cookies.privateKey);
    let sendData = client.sendData(JSON.stringify(payloadObj));
    sendData.then(result => {
      res.send(JSON.stringify({status: result}));
    });
  }
});

/* Update Patient Details */
router.post('/update', function(req, res, next) {
  if(!req.cookies.privateKey) {
    res.redirect('/');
    return;
  }
  let payloadObj = req.body;
  payloadObj.district = req.cookies.district;
  payloadObj.hospitalName = req.cookies.hospitalName;
  if(payloadObj.patientID) {
    payloadObj.action = "updateEmergencyPatient";
  } else {
    payloadObj.action = "updateRoutinePatient";
  }
  console.log('CLIENTUPDATEPAYLOADACTION: '+payloadObj.action);
  if(payloadObj.action) {
    let client = new Client(req.cookies.privateKey);
    let sendData = client.sendData(JSON.stringify(payloadObj));
    sendData.then(result => {
      console.log(JSON.stringify(result));
      res.redirect('/patient');
    });
  }
});

/* Add Patient Visits */
router.post('/visits/add', function(req, res, next) {
    try {
        if(!req.cookies.privateKey) {
            res.redirect('/');
            return;
        }
        let payloadObj = req.body;
        payloadObj.action = "addVisitsDetails";
        let client = new Client(req.cookies.privateKey);
        let sendData = client.sendData(JSON.stringify(payloadObj));
        sendData.then(result => {
            res.redirect('/patient');
        });
    }
    catch(error) {
        console.log(error);
    }
});

/* Update Patient Visits */
router.post('/visits/update', function(req, res, next) {
    if(!req.cookies.privateKey) {
        res.redirect('/');
        return;
    }
    let payloadObj = req.body;
    payloadObj.action = "updateVisitsDetails";
    let client = new Client(req.cookies.privateKey);
    let sendData = client.sendData(JSON.stringify(payloadObj));
    sendData.then(result => {
        console.log(JSON.stringify(result));
        res.redirect('/patient');
    });
});

/* Add Patient Tests */
router.post('/tests/add', function(req, res, next) {
    if(!req.cookies.privateKey) {
        res.redirect('/');
        return;
    }
    let payloadObj = req.body;
    payloadObj.action = "addTestsDetails";
    let client = new Client(req.cookies.privateKey);
    let sendData = client.sendData(JSON.stringify(payloadObj));
    sendData.then(result => {
        console.log(JSON.stringify(result));
        res.redirect('/patient');
    });
});

/* Update Patient Tests */
router.post('/tests/update', function(req, res, next) {
    if(!req.cookies.privateKey) {
        res.redirect('/');
        return;
    }
    let payloadObj = req.body;
    payloadObj.action = "updateTestsDetails";
    let client = new Client(req.cookies.privateKey);
    let sendData = client.sendData(JSON.stringify(payloadObj));
    sendData.then(result => {
        console.log(JSON.stringify(result));
        res.redirect('/patient');
    });
});

module.exports = router;