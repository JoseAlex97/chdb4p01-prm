const {createHash} = require('crypto');
const {CryptoFactory, createContext} = require('sawtooth-sdk/signing');
const protobuf = require('sawtooth-sdk/protobuf');
const fetch = require('node-fetch');
const {Secp256k1PrivateKey} = require('sawtooth-sdk/signing/secp256k1');
const {TextEncoder, TextDecoder} = require('text-encoding/lib/encoding');

const FAMILY_NAME="hospital";
const FAMILY_VERSION="1.0";
const NAMESPACE = hash("Patient Record Management").substring(0,6);

function hash(v) {
    return createHash('sha512').update(v).digest('hex');
}

class Client {
    constructor(privateKey){
        const context = createContext('secp256k1');
        const secp256k1pk = Secp256k1PrivateKey.fromHex(privateKey);
        this.signer = new CryptoFactory(context).newSigner(secp256k1pk);
        this.publicKey = this.signer.getPublicKey().asHex();
        this.address = NAMESPACE;
    }
    async getAddress(address) {
        try {
            let url = '';
            if(address) {
                url = address.length == 70 ? 'http://rest-api:8008/state/' + address : 'http://rest-api:8008/state?address=' + address;
            }
            let response = await fetch(url,{ method: 'GET', });
            let responseJson = await response.json();
            console.log('GETADDRESS SUCCESS: ' + responseJson);
            return responseJson;
        }
        catch(error){
            console.error(error);
        }
    }
    async getData(address) {
        try {
            let url = '';
            if(address) {
                url = address.length == 70 ? 'http://rest-api:8008/state/' + address : 'http://rest-api:8008/state?address=' + address;
            }
            let response = await fetch(url,{ method: 'GET', });
            let responseJson = await response.json();
            if(Array.isArray(responseJson.data)) {
                let multiAddressArray = [];
                for(let singleAddressArray of responseJson.data) {
                    multiAddressArray.push(new Buffer(singleAddressArray.data,'base64').toString());
                }                
                return { "data": multiAddressArray };
            } else {
                return (responseJson.data ? new Buffer(responseJson.data,'base64').toString() : JSON.stringify(responseJson));
            }            
        }
        catch(error){
            console.error(error);
        }
    }
    async getDataFullAddress(address) {
        let addressData = await fetch('http://rest-api:8008/state/' + address);
        let addressDataJson = await addressData.json();
        if(addressDataJson.data) {
            let addressDataObj = JSON.parse(new Buffer(addressDataJson.data,'base64').toString());
            // Check if address is pointing (linked) to another address
            if(addressDataObj.goto) {
                // Get & return data from linked address
                let linkedAddressData = await fetch('http://rest-api:8008/state/' + addressDataObj.goto);
                let linkedAddressDataJson = await linkedAddressData.json();
                return JSON.stringify(linkedAddressDataJson.data ? {data: new Buffer(linkedAddressDataJson.data,'base64').toString()} : linkedAddressResponseJson);
            } else {
                // Get & return data from address
                return JSON.stringify({data: new Buffer(addressDataJson.data,'base64').toString()});
            }
        } else {
            return JSON.stringify(addressData);
        }
    }
    async getVisitsInfo(address) {
        try {
            let patient = {}, visits = {};
            // Get Patient details from address
            let addressData = await fetch('http://rest-api:8008/state/' + address);
            let addressDataJson = await addressData.json();
            if(addressDataJson.data) {
                let addressDataObj = JSON.parse(new Buffer(addressDataJson.data,'base64').toString());
                // Check if address is pointing (linked) to another address
                if(addressDataObj.goto) {
                    // Get & return data from linked address
                    let linkedAddressData = await fetch('http://rest-api:8008/state/' + addressDataObj.goto);
                    let linkedAddressDataJson = await linkedAddressData.json();
                    if(linkedAddressDataJson.data) {
                        patient.address = addressDataObj.goto;
                        patient.data = new Buffer(linkedAddressDataJson.data,'base64').toString();
                    }
                } else {
                    // Get & return data from address                    
                    patient.address = address;
                    patient.data = new Buffer(addressDataJson.data,'base64').toString();
                }
            } else {
                return JSON.stringify({error: 'No such patient'});
            }
            // Get Visit details
            if(patient.address) {
                let patientVisitAddress = patient.address.replace(NAMESPACE+'00',NAMESPACE+'01').substring(0,64);
                let visitAddressData = await fetch('http://rest-api:8008/state?address=' + patientVisitAddress);
                let visitAddressDataJson = await visitAddressData.json();
                let visits = [];
                if(visitAddressDataJson.data) {
                    for(let visitsData of visitAddressDataJson.data) {
                        let visitsObj = {};
                        let decodedVisitsDataObj = JSON.parse(new Buffer(visitsData.data,'base64').toString());
                        for(let visitData of decodedVisitsDataObj) {
                            visitsObj = visitData;
                            visitsObj.patientVisitAddress = visitsData.address;
                            visits.push(visitsObj);
                        }
                    }
                }
                return JSON.stringify({patient: patient, visits: visits});
            }
        }
        catch(error){
            console.error(error);
        }
        
    }

    async getTestsInfo(address) {
        try {
            let patient = {}, tests = {};
            // Get Patient details from address
            let addressData = await fetch('http://rest-api:8008/state/' + address);
            let addressDataJson = await addressData.json();
            if(addressDataJson.data) {
                let addressDataObj = JSON.parse(new Buffer(addressDataJson.data,'base64').toString());
                // Check if address is pointing (linked) to another address
                if(addressDataObj.goto) {
                    // Get & return data from linked address
                    let linkedAddressData = await fetch('http://rest-api:8008/state/' + addressDataObj.goto);
                    let linkedAddressDataJson = await linkedAddressData.json();
                    if(linkedAddressDataJson.data) {
                        patient.address = addressDataObj.goto;
                        patient.data = new Buffer(linkedAddressDataJson.data,'base64').toString();
                    }
                } else {
                    // Get & return data from address                    
                    patient.address = address;
                    patient.data = new Buffer(addressDataJson.data,'base64').toString();
                }
            } else {
                return JSON.stringify({error: 'No such patient'});
            }
            // Get Test details
            if(patient.address) {
                let patientTestAddress = patient.address.replace(NAMESPACE+'00',NAMESPACE+'02').substring(0,64);
                let testAddressData = await fetch('http://rest-api:8008/state?address=' + patientTestAddress);
                let testAddressDataJson = await testAddressData.json();
                let tests = [];
                if(testAddressDataJson.data) {
                    for(let testsData of testAddressDataJson.data) {
                        let testsObj = {};
                        let decodedTestsDataObj = JSON.parse(new Buffer(testsData.data,'base64').toString());
                        for(let testData of decodedTestsDataObj) {
                            testsObj = testData;
                            testsObj.patientTestAddress = testsData.address;
                            tests.push(testsObj);
                        }
                    }
                }
                return JSON.stringify({patient: patient, tests: tests});
            }
        }
        catch(error){
            console.error(error);
        }
        
    }
    async getDataPatientVisit(address, visitID) {
        let addressData = await fetch('http://rest-api:8008/state/' + address);
        let addressDataJson = await addressData.json();
        let decodedAddressData = new Buffer(addressDataJson.data,'base64').toString();
        let decodedAddressDataObj = JSON.parse(decodedAddressData);
        for(let visit of decodedAddressDataObj) {
            if(visit.visitID == visitID) {
                return visit;
            }
        }
    }
    async getDataPatientTest(address, testID) {
        let addressData = await fetch('http://rest-api:8008/state/' + address);
        let addressDataJson = await addressData.json();
        let decodedAddressData = new Buffer(addressDataJson.data,'base64').toString();
        let decodedAddressDataObj = JSON.parse(decodedAddressData);
        for(let test of decodedAddressDataObj) {
            if(test.testID == testID) {
                return test;
            }
        }
    }
    async sendData(payload) {
        let encode =new TextEncoder('utf8');
        const payloadBytes = encode.encode(payload);
        const transactionHeaderBytes = protobuf.TransactionHeader.encode({
            familyName: FAMILY_NAME,
            familyVersion: FAMILY_VERSION,
            inputs: [this.address],
            outputs: [this.address],
            signerPublicKey: this.publicKey,
            nonce: "" + Math.random(),
            batcherPublicKey: this.publicKey,
            dependencies: [],
            payloadSha512: hash(payloadBytes)
        }).finish();
        const transaction = protobuf.Transaction.create({
            header: transactionHeaderBytes,
            headerSignature: this.signer.sign(transactionHeaderBytes),
            payload: payloadBytes
        });
        const transactions = [transaction];
        const batchHeaderBytes = protobuf.BatchHeader.encode({
            signerPublicKey: this.publicKey,
            transactionIds: transactions.map((txn) => txn.headerSignature),
        }).finish();
        const batch = protobuf.Batch.create({
            header: batchHeaderBytes,
            headerSignature: this.signer.sign(batchHeaderBytes),
            transactions: transactions,
        });
        const batchListBytes = protobuf.BatchList.encode({
            batches: [batch]
        }).finish();
        let response = await fetch('http://rest-api:8008/batches', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/octet-stream'
            },
            body: batchListBytes
        });
        return response.status;
    }
    async getPatients() {
        let address = NAMESPACE + '00';
        let patientsData = await this.getAddress(address);
        let patientList = [];
        for(let patient of patientsData.data) {
            let patientDataObj = JSON.parse(new Buffer(patient.data,'base64').toString());
            patientDataObj.blockchainAddress = patient.address;
            patientList.push(patientDataObj);
        }
        return JSON.stringify(patientList);
    }
    getEmergencyPatientAddress(district, hospitalName, patientID) {
        return NAMESPACE + '0000' + hash(district).substring(0,18) + hash(hospitalName).substring(0,18) + hash(patientID).substring(0,18) + '000000';
    }
    getRoutinePatientAddress(patientName, dob, parentName) {
        return NAMESPACE + '0001' + dob + hash(patientName).substring(0,24) + hash(parentName).substring(0,24) + '000000';
    }
}
module.exports.Client = Client;