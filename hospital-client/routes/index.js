var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  if(!req.cookies.privateKey) {
    res.render('index');
    return;
  }
  res.render('loggedin');
});

/* POST home page. */
router.post('/', function(req, res, next) {
  if(!req.body.privateKey) {
    res.redirect('/');
  }
  let allowedKeys = [];
  //Check if valid private key
  res.cookie('privateKey', req.body.privateKey);
  res.cookie('district', req.body.district);
  res.cookie('hospitalName', req.body.hospitalName);
  res.render('loggedin');
});

module.exports = router;
