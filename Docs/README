PROJECT NAME
  Patient Record Management

SHORT DESCRIPTION
  Government based Sawtooth Application for centralised access of medical records of patients who have availed of services of Government Health Providers.
  The aim of this application is to reduce costs and inefficiencies in medical care by:
    - Reducing duplication of medical tests
    - Provide complete medical history on demand
    - Provide real time reports to government for use during large scale disasters, and epidemics.


LONG DESCRIPTION
Maintaining a record is an essential part of every system.Like every systems records are used in medical and health care sector for smooth functioning of the system and to serve the patients in an effective and hassle-free way.An individual's medical history becomes very important during the time of health emergencies and other medical needs.So keeping track of medical history is a crucial part of proper healthcare.When it comes to the contents of one's medical record, there are various type of information present in it.These medical information are obtained by conducting certain lab tests.These tests can be expensive depending upon the complexity and seriousness of the test.Also, for a proper diagnosing of the patient, a doctor will have to suggest many tests for obtaining required diagnosing parameters.Most of these tests are inevitable So, the patient has to undergo them and obtain results.These results are often LOST from patients over time.When they need to consult a doctor in future, the patient will have to again undergo the so called tests, which could have been avoided if they still had the test results.Also,The previous test results can be helpful if one patient is consulting a new doctor in the FUTURE.The medical history can help the new doctor figure out how to treat the patient easily.Whenever the doctor needs a patient's medical history, it can be accessed ON DEMAND from a Patient record management system(P.R.M.S) implemented using Hyperledger Sawtooth framework.

During the time of National,State-Wide health emergencies govt authorities can benefit from a record keeping system like P.R.M.S in a great way.When accessed,the authorities can treat every patient with ease by the help of each and everyone's medical history stored in PRMS.Once the results are stored in blockchain they cant be tampered with.These data will be safe in PRMS.



INSTALLATION INSTRUCTIONS
=========================


  SET UP GIT :
      -For detailed installation instructions, Go to: https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup

      NOTICE:FOR RUNNING GITLAB MINIMUM 4GB RAM IS NEEDED
    
  
  SET UP DOCKER :
      -For detailed installation instructions, Go to: https://docs.docker.com/compose/gettingstarted/
      
      NOTICE: MAKE SURE YOU DONT HAVE ANY EXISTING CONTAINERS.IF YOU DO, DELETE THEM.


  STEPS FOR SETTING UP THE APPLICATION :
  - Download the application files,Use the following command:

    git clone https://gitlab.com/kba-chd-prm/prm.git

  - Go to root folder(prm) and run following commands:

    sudo docker-compose build
    sudo docker-compose up

    As the last command is executed the application will be up and running.
    For Detailed operational instructions of the application refer DOCUMENTATION FILE.


  PERMISSIONING :
    The FAMILY_NAME "hospital" should only be accessed by hospital users.
        ALLOWED PUBLIC KEYS:
        036bfae8d74cb4ac52e1eb39f9c987e9225d20761f0b137b2fb2304b24768d1aea
        0206f7e93190437448d7937f0112c963f7ddd0cdd373ed8ec136a46a26469e7732
        More keys can be added as per requirements
    The FAMILY_NAME "non-hospital" should only be accessed by non-hospital users.
        ALLOWED PUBLIC KEYS:
        03cdc6dd96ed0eb3306de4963acbfbe08c762d6a66b71632d0d7322752d8f1c65f
        More keys can be added as per requirements
    PERMISSIONING PROCEDURE:
    After starting the applicaion, open a new Terminal window, and enter the two commands one at time:
    1)  docker exec -it validator bash
    2)  sawset proposal create --key ~/.sawtooth/keys/my_key.priv sawtooth.identity.allowed_keys=$(cat ~/.sawtooth/keys/my_key.pub) --url http://rest-api:8008 &&
        sawtooth identity policy create --key ~/.sawtooth/keys/my_key.priv hospital_users "PERMIT_KEY 036bfae8d74cb4ac52e1eb39f9c987e9225d20761f0b137b2fb2304b24768d1aea" "PERMIT_KEY 0206f7e93190437448d7937f0112c963f7ddd0cdd373ed8ec136a46a26469e7732" --url http://rest-api:8008 &&
        sawtooth identity role create --key ~/.sawtooth/keys/my_key.priv transactor.transaction_signer.hospital hospital_users --url http://rest-api:8008 &&
        sawtooth identity policy create --key ~/.sawtooth/keys/my_key.priv non-hospital_users "PERMIT_KEY 03cdc6dd96ed0eb3306de4963acbfbe08c762d6a66b71632d0d7322752d8f1c65f" --url http://rest-api:8008 &&
        sawtooth identity role create --key ~/.sawtooth/keys/my_key.priv transactor.transaction_signer.non-hospital non-hospital_users --url http://rest-api:8008
