const {
    Message,
    EventFilter,
    EventList,
    EventSubscription,
    ClientEventsSubscribeRequest,
    ClientEventsSubscribeResponse
    } = require('sawtooth-sdk/protobuf');
const { Stream } = require('sawtooth-sdk/messaging/stream');
const {CryptoFactory, createContext} = require('sawtooth-sdk/signing');
const {Secp256k1PrivateKey} = require('sawtooth-sdk/signing/secp256k1');
const WebSocket = require('ws');

const validatorURL = "tcp://validator:4004";
 
const ws = new WebSocket.Server({ port: 80 }); 
ws.on('connection', function connection(ws) {
  ws.on('message', function incoming(message) {
    let publicKey;
    let messageObj = JSON.parse(message);
    let filters = [];
    for(let filter of messageObj.filters) {
        if(filter.key == 'privateKey') {
            const context = createContext('secp256k1');
            const secp256k1pk = Secp256k1PrivateKey.fromHex(filter.matchString);
            const signer = new CryptoFactory(context).newSigner(secp256k1pk);
            publicKey = signer.getPublicKey().asHex();
            filters.push(EventFilter.create({
                key: 'publicKey',
                matchString: publicKey,
                filterType: EventFilter.FilterType[filter.filterType]
            }));
        } else {
            filters.push(EventFilter.create({
                key: filter.key,
                matchString: filter.matchString,
                filterType: EventFilter.FilterType[filter.filterType]
            }));
        }
    }
    const subscription = EventSubscription.create({ eventType: messageObj.eventType, filters: filters });
    const subscription_request = ClientEventsSubscribeRequest.encode({
        subscriptions : [subscription]
    }).finish()
    let stream = new Stream(validatorURL);
    stream.connect(()=> {
        stream.send(Message.MessageType.CLIENT_EVENTS_SUBSCRIBE_REQUEST,subscription_request)
        .then(response => ClientEventsSubscribeResponse.decode(response))
        .then(decodedResponse => {
            if(decodedResponse.status === 0 || decodedResponse.status === 1) {
                ws.send(JSON.stringify({status: decodedResponse.status}));
            } else {
                ws.send(JSON.stringify({status: 'fail'}));
            }
        })        
        stream.onReceive(getNotification);
        function getNotification(message) {
            let eventList =  EventList.decode(message.content).events;
            eventList.map(function(event){
                if(event.eventType === messageObj.eventType) {
                  for(let attribute of event.attributes) {
                    if(attribute.key === 'publicKey' && attribute.value === publicKey) {
                      ws.send(JSON.stringify(event));
                      ws.close();
                    }
                  }
                }
            })
        }
    })
  });
});